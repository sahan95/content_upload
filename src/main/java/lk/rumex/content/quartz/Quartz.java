package lk.rumex.content.quartz;

import lk.rumex.content.dto.SubscriptionDTO;
import lk.rumex.content.entity.Subscription;
import lk.rumex.content.quartz.jobs.SubstriptionJob;
import org.quartz.*;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.UUID;

@Component
public class Quartz {


    public JobDetail jobDetail(SubscriptionDTO subscriptionDTO){

        System.out.println(UUID.randomUUID().toString());
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("subscription",subscriptionDTO);
        return JobBuilder.newJob(SubstriptionJob.class)
                .withIdentity(UUID.randomUUID().toString(),"Subscription Jobs")
                .withDescription("Make Subscription")
                .usingJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    public Trigger trigger(JobDetail jobDetail, ZonedDateTime time){
        System.out.println(Date.from(time.toInstant()));
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName(),"Subscription Triggers")
                .withDescription("Make Subscription Trigger")
                .startAt(Date.from(time.toInstant()))
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
                .build();
    }
}
