package lk.rumex.content.quartz.jobs;

import lk.rumex.content.dto.SubscriptionDTO;
import lk.rumex.content.entity.Subscription;
import lk.rumex.content.service.SubscriptionService;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class SubstriptionJob extends QuartzJobBean {


    @Autowired
    private SubscriptionService subscriptionService;


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        SubscriptionDTO subscription = (SubscriptionDTO)jobDataMap.get("subscription");
        subscriptionService.save(subscription);
    }
}
