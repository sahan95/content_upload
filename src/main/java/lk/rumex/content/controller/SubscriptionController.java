package lk.rumex.content.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lk.rumex.content.dto.SubscriptionDTO;
import lk.rumex.content.quartz.Quartz;
import lk.rumex.content.service.SubscriptionService;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@CrossOrigin
@RestController
@RequestMapping(value = "api/v1/subscriptions")
@Api(value = "Subscription Control API", description = "Operation for Subscription")
public class SubscriptionController {

    @Autowired
    private SubscriptionService subscriptionService;


    @Autowired
    private Scheduler scheduler;


    @Autowired
    private Quartz quartz;
    @ApiOperation(value = "Upload Subscription to Database")
    @PutMapping
    public void save(@RequestBody SubscriptionDTO subscriptionDTO) throws SchedulerException {
        ZoneId of1 = ZoneId.of("Asia/Colombo");
        ZonedDateTime zonedDateTime = ZonedDateTime.of(2019, 05, 24, 15, 50, 00, 00, of1);
        ZonedDateTime zonedDateTime1 = ZonedDateTime.of(2019, 05, 24, 15, 51, 00, 00, of1);

        StdSchedulerFactory stdSchedulerFactory = new StdSchedulerFactory();
        scheduler = stdSchedulerFactory.getScheduler();

        Map<JobDetail,Set<Trigger>> triggersAndJobs = new HashMap<>();
        JobDetail jobDetail = quartz.jobDetail(subscriptionDTO);
        Set<Trigger> triggers =new HashSet<>();

        CronTrigger t1 = ( CronTrigger ) new Quartz().trigger( jobDetail, zonedDateTime ) ;
        CronTrigger t2 = ( CronTrigger ) new Quartz().trigger( jobDetail, zonedDateTime1 ) ;

        triggers.add( t1 ) ;
        triggers.add( t2 ) ;

        triggersAndJobs.put(jobDetail,triggers);

        scheduler.scheduleJobs(triggersAndJobs,true);


//
//        System.out.println(of1);
//        ZonedDateTime zonedDateTime = ZonedDateTime.of(2019, 05, 24, 15, 26, 00, 00, of1);
//        ZonedDateTime zonedDateTime1 = ZonedDateTime.of(2019, 05, 24, 15, 27, 00, 00, of1);
//
//        Trigger trigger = quartz.trigger(jobDetail, zonedDateTime);
//        Trigger trigger1 = quartz.trigger(jobDetail, zonedDateTime1);
//        scheduler.scheduleJob(jobDetail,trigger);
//        scheduler.scheduleJob(jobDetail,trigger1);

    }

    @ApiOperation(value = "Update content according to the ID")
    @PostMapping(value = "/{ID}")
    public void update(@PathVariable("ID") int ID, @RequestBody SubscriptionDTO subscriptionDTO){

        subscriptionService.update(ID,subscriptionDTO);
    }
    @ApiOperation(value = "Delete Content")
    @DeleteMapping(value = "/{ID}")
    public void delete(@PathVariable("ID") int ID){
        subscriptionService.delete(ID);
    }

    @ApiOperation(value = "Get Content by ID")
    @GetMapping(value = "/{ID}")
    public SubscriptionDTO findByID(@PathVariable("ID") int ID){
        return  subscriptionService.findByID(ID);
    }
    @ApiOperation(value = "Get all Contents")
    @GetMapping(value = "/{start}/{end}")
    public List<SubscriptionDTO> findAll(@PathVariable("start") int start, @PathVariable("end") int end){
        return subscriptionService.findAll(start, end);



    }

}
