package lk.rumex.content.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lk.rumex.content.dto.ContentDTO;
import lk.rumex.content.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "api/v1/content")
@Api(value = "Content Upload System",description = "Operations for controlling contents ")
public class Contentcontroller {

    @Autowired
    private ContentService contentService;

    @ApiOperation(value = "Upload Content to the Database")
    @PutMapping
    public void uploadContent(@RequestBody ContentDTO contentDTO){
        contentService.save(contentDTO);
    }


    @ApiOperation(value = "Update content according to the ID")
    @PostMapping(value = "/{ID}")
    public void updateContent(@PathVariable("ID") int ID, @RequestBody ContentDTO contentDTO){
        contentService.update(ID,contentDTO);
    }

    @ApiOperation(value = "Delete Content")
    @DeleteMapping(value = "/{ID}")
    public void deleteContent(@PathVariable("ID") int ID){
        contentService.delete(ID);
    }

    @ApiOperation(value = "Get Content by ID",response = ContentDTO.class)
    @GetMapping(value = "/{ID}")
    public ContentDTO findByID(@PathVariable("ID") int ID){

        return contentService.findByID(ID);
    }


    @ApiOperation(value = "Get all Contents")
    @GetMapping(value = "/{start}/{end}")
    public List<ContentDTO> findAll(@PathVariable("start") int start, @PathVariable("end") int end){
        return contentService.getAll(start,end);
    }



}
