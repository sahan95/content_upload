package lk.rumex.content.service.impl;

import lk.rumex.content.dto.SubscriptionDTO;
import lk.rumex.content.entity.Subscription;
import lk.rumex.content.repository.SubscriptionRepository;
import lk.rumex.content.service.SubscriptionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class SubscriptionServiceImpl implements SubscriptionService {

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Override
    public void save(SubscriptionDTO subscriptionDTO) {
        Subscription subscription = new Subscription();
        BeanUtils.copyProperties(subscriptionDTO,subscription);
        subscriptionRepository.save(subscription);
    }

    @Override
    public void update(int ID, SubscriptionDTO subscriptionDTO) {

        if(!subscriptionRepository.findById(ID).isPresent()){
            throw new RuntimeException("Subscription Not Found ! ");
        }
        Subscription subscription = subscriptionRepository.findById(ID).get();
        BeanUtils.copyProperties(subscriptionDTO,subscriptionDTO);

    }

    @Override
    public void delete(int ID) {
        if(!subscriptionRepository.findById(ID).isPresent()){
            throw new RuntimeException("Subscription Not Found ! ");
        }
        subscriptionRepository.deleteById(ID);
    }

    @Override
    public SubscriptionDTO findByID(int ID) {
        if(!subscriptionRepository.findById(ID).isPresent()){
            throw new RuntimeException("Subscription Not Found ! ");
        }
        SubscriptionDTO subscriptionDTO = new SubscriptionDTO();
        Subscription subscription = subscriptionRepository.findById(ID).get();
        BeanUtils.copyProperties(subscription,subscriptionDTO);
        return subscriptionDTO;
    }

    @Override
    public List<SubscriptionDTO> findAll(int start, int end) {

        PageRequest pageRequest = PageRequest.of(start, end);
        Page<Subscription> subscriptions = subscriptionRepository.findAll(pageRequest);
        List<SubscriptionDTO> subscriptionDTOS = new ArrayList<>();
        subscriptions.forEach(subscription -> {
            SubscriptionDTO subscriptionDTO = new SubscriptionDTO();
            BeanUtils.copyProperties(subscription,subscriptionDTO);

            subscriptionDTOS.add(subscriptionDTO);

        });

        return subscriptionDTOS;

    }
}
