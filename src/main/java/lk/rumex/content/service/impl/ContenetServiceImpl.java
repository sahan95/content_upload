package lk.rumex.content.service.impl;

import lk.rumex.content.dto.ContentDTO;
import lk.rumex.content.entity.Content;
import lk.rumex.content.repository.ContentRepository;
import lk.rumex.content.service.ContentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class ContenetServiceImpl implements ContentService {
    @Autowired
    private ContentRepository contentRepository;


    @Override
    public void save(ContentDTO contentDTO) {
        Content content = new Content();
        BeanUtils.copyProperties(contentDTO,content);
        contentRepository.save(content);

    }

    @Override
    public void update(int ID, ContentDTO contentDTO) {
        if(!contentRepository.findById(ID).isPresent()){
            throw new RuntimeException("No Content Found !");
        }
        Content content = contentRepository.findById(ID).get();
        BeanUtils.copyProperties(contentDTO,content);

    }

    @Override
    public void delete(int ID) {
        if(!contentRepository.findById(ID).isPresent()){
            throw new RuntimeException("No Content Found !");
        }
        contentRepository.deleteById(ID);
    }

    @Override
    public ContentDTO findByID(int ID) {

        if(!contentRepository.findById(ID).isPresent()){
            throw new RuntimeException("No Content Found !");
        }

        Content content = contentRepository.findById(ID).get();
        ContentDTO contentDTO = new ContentDTO();
        BeanUtils.copyProperties(content,contentDTO);
        return contentDTO;
    }

    @Override
    public List<ContentDTO> getAll(int start, int end) {

        PageRequest pageRequest = PageRequest.of(start, end);
        Page<Content> allContent = contentRepository.findAll(pageRequest);
        System.out.printf(allContent.toString());
        List<ContentDTO> contentDTOS = new ArrayList<>();


        allContent.forEach(content -> {
            ContentDTO contentDTO = new ContentDTO();
            BeanUtils.copyProperties(content,contentDTO);
            contentDTOS.add(contentDTO);
        });

        return contentDTOS;
    }


}
