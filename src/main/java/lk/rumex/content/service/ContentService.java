package lk.rumex.content.service;

import lk.rumex.content.dto.ContentDTO;

import java.awt.print.Pageable;
import java.util.List;

public interface ContentService {


    void save(ContentDTO contentDTO);

    void update(int ID, ContentDTO contentDTO);

    void  delete(int ID);

    ContentDTO findByID(int ID);

    List<ContentDTO> getAll(int start,int end);



}
