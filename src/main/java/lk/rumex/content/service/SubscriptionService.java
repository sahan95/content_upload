package lk.rumex.content.service;

import lk.rumex.content.dto.SubscriptionDTO;

import java.util.List;

public interface SubscriptionService {


    void save(SubscriptionDTO subscriptionDTO);

    void update(int ID, SubscriptionDTO subscriptionDTO);

    void delete(int ID);

    SubscriptionDTO findByID(int ID);

    List<SubscriptionDTO> findAll(int start, int end);



}
