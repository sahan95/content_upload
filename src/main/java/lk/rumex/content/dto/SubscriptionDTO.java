package lk.rumex.content.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

public class SubscriptionDTO implements Serializable {

    private
    int ID;
    private String MSISDN;
    private String staus;
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd-MM-yyyy")
    private
    Date createdDate;
    private String agent;


    public SubscriptionDTO(int ID, String MSISDN, String staus, Date createdDate, String agent) {
        this.ID = ID;
        this.MSISDN = MSISDN;
        this.staus = staus;
        this.createdDate = createdDate;
        this.agent = agent;
    }

    public SubscriptionDTO() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getMSISDN() {
        return MSISDN;
    }

    public void setMSISDN(String MSISDN) {
        this.MSISDN = MSISDN;
    }

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    @Override
    public String toString() {
        return "SubscriptionDTO{" +
                "ID=" + ID +
                ", MSISDN='" + MSISDN + '\'' +
                ", staus='" + staus + '\'' +
                ", createdDate=" + createdDate +
                ", agent='" + agent + '\'' +
                '}';
    }
}
