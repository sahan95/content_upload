package lk.rumex.content.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ContentDTO {

    private
    int ID;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private
    Date uploadDate;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private
    Date deliverDate;
    private String agent;
    private String content;


    public ContentDTO(Date uploadDate, Date deliverDate, String agent, String content) {
        this.setUploadDate(uploadDate);
        this.setDeliverDate(deliverDate);
        this.setAgent(agent);
        this.setContent(content);
    }

    public ContentDTO() {
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Date getDeliverDate() {
        return deliverDate;
    }

    public void setDeliverDate(Date deliverDate) {
        this.deliverDate = deliverDate;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "ContentDTO{" +
                "ID=" + ID +
                ", uploadDate=" + uploadDate +
                ", deliverDate=" + deliverDate +
                ", agent='" + agent + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
