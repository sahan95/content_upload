package lk.rumex.content.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE,generator = "G1")
    @TableGenerator(name = "G1",table = "AutoIncrement",allocationSize = 1,valueColumnName = "nextID")
    private
    int ID;
    private String MSISDN;
    private String staus;
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd-MM-yyyy")
    private
    Date createdDate;
    private String agent;

    public Subscription(String MSISDN, String staus, Date createdDate, String agent) {
        this.setMSISDN(MSISDN);
        this.setStaus(staus);
        this.setCreatedDate(createdDate);
        this.setAgent(agent);
    }

    public Subscription() {
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getMSISDN() {
        return MSISDN;
    }

    public void setMSISDN(String MSISDN) {
        this.MSISDN = MSISDN;
    }

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "ID=" + ID +
                ", MSISDN='" + MSISDN + '\'' +
                ", staus='" + staus + '\'' +
                ", createdDate=" + createdDate +
                ", agent='" + agent + '\'' +
                '}';
    }
}
